package ru.gryazev.tm.constant;

import org.jetbrains.annotations.NotNull;

public class LogFileName {

    @NotNull
    private static final String LOG_DIR = System.getProperty("user.dir") + "/tm-logger/log/";

    @NotNull
    public static final String ERROR_LOG = LOG_DIR + "error.txt";

    @NotNull
    public static final String PROJECT_LOG = LOG_DIR + "project.txt";

    @NotNull
    public static final String TASK_LOG = LOG_DIR + "task.txt";

    @NotNull
    public static final String USER_LOG = LOG_DIR + "user.txt";

    @NotNull
    public static final String SESSION_LOG = LOG_DIR + "session.txt";

}
