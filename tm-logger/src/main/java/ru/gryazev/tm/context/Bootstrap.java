package ru.gryazev.tm.context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.ILoggingService;
import ru.gryazev.tm.constant.LogFileName;
import ru.gryazev.tm.listener.LogMessageListener;
import ru.gryazev.tm.service.LoggingService;
import ru.gryazev.tm.service.ReceiverService;

import javax.jms.JMSException;
import javax.jms.TextMessage;

public class Bootstrap {

    @NotNull
    private final ReceiverService receiverService;

    @NotNull
    private final ILoggingService loggingService;

    @NotNull
    private final LogMessageListener logMessageListener;

    public Bootstrap() throws JMSException {
        this.receiverService = new ReceiverService();
        this.loggingService = new LoggingService();
        this.logMessageListener = new LogMessageListener(loggingService);
        receiverService.getConsumer().setMessageListener(logMessageListener);
    }

}
