package ru.gryazev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.ILoggingService;
import ru.gryazev.tm.constant.LogFileName;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class LoggingService implements ILoggingService {

    public void writeLog(@NotNull final TextMessage textMessage) throws IOException, JMSException {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String message = textMessage.getText() + "\n\n";
        @NotNull final ObjectNode node = mapper.readValue(message, ObjectNode.class);
        if (!node.has("className")) return;
        @Nullable final String className = node.get("className").asText();
        @Nullable final String fileName = getFileName(className);
        if (fileName == null) return;
        @NotNull final File file = new File(fileName);
        file.getParentFile().mkdir();
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        fileOutputStream.write(message.getBytes());
        fileOutputStream.close();
    }

    @Nullable
    private String getFileName(@Nullable final String className) {
        if (className == null) return null;
        switch (className) {
            case "SessionEntity": return LogFileName.SESSION_LOG;
            case "UserEntity": return LogFileName.USER_LOG;
            case "ProjectEntity": return LogFileName.PROJECT_LOG;
            case "TaskEntity": return LogFileName.TASK_LOG;
            default: return null;
        }
    }

}
