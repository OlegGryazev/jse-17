package ru.gryazev.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.CommandLocator;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.context.SessionLocator;
import ru.gryazev.tm.api.repository.ISettingRepository;
import ru.gryazev.tm.api.service.ISettingService;
import ru.gryazev.tm.api.service.ITerminalService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.command.task.AbstractTaskCommand;
import ru.gryazev.tm.endpoint.*;
import ru.gryazev.tm.repository.SettingRepository;
import ru.gryazev.tm.service.SettingService;
import ru.gryazev.tm.service.TerminalService;

import java.io.IOException;
import java.lang.Exception;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public final class Bootstrap implements ServiceLocator, SessionLocator, CommandLocator {

    @Getter
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final ISettingRepository settingRepository = new SettingRepository();

    @Getter
    @NotNull
    private final ISettingService settingService = new SettingService(settingRepository);

    @Getter
    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @Getter
    @Setter
    @Nullable
    private String token;

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init(@NotNull final Set<Class<? extends AbstractCommand>> commandClasses) {
        terminalService.print("**** Welcome to Project Manager ****");
        commandsInit(commandClasses);
        while (true) {
            try {
                @Nullable final AbstractCommand command = getCommand();
                if (command == null) continue;
                command.execute();
            } catch (Exception e) {
                terminalService.print(e.getMessage());
            }
        }
    }

    private void commandsInit(@NotNull final Set<Class<? extends AbstractCommand>> commandClasses) {
        for (@NotNull final Class<? extends AbstractCommand> clazz : commandClasses){
            if (clazz == AbstractTaskCommand.class) continue;
            @Nullable AbstractCommand command = null;
            try {
                command = clazz.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            if (command == null) continue;
            command.setServiceLocator(this);
            command.setTerminalService(terminalService);
            command.setSettingService(settingService);
            command.setSessionLocator(this);
            command.setCommandLocator(this);
            commands.put(command.getName(), command);
        }
    }

    @Nullable
    private AbstractCommand getCommand() throws IOException {
        @Nullable final AbstractCommand command = commands.get(terminalService.readCommand());
        if (command == null) {
            terminalService.print("Command not found!");
            return null;
        }
        return command;
    }

    @Override
    public @NotNull Map<String, AbstractCommand> getCommands() {
        return commands;
    }

}