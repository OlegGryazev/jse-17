package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.util.List;

@NoArgsConstructor
public class TaskFindByDetails extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-find-details";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "List of tasks by details or part of details.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final String token = getToken();
        @NotNull final String taskDetails = terminalService.getSearchString();
        @NotNull final List<Task> tasks = serviceLocator.getTaskEndpoint()
                .findTaskByDetails(token,taskDetails);
        if (tasks.isEmpty()) throw new CrudListEmptyException();
        for (int i = 0; i < tasks.size(); i++) {
            @Nullable final Project project = serviceLocator.getProjectEndpoint().findOneProject(token, tasks.get(i).getProjectId());
            @NotNull final String linkedTo = (project == null) ?
                    "unlinked" : "linked to project " + project.getName();
            terminalService.print((i + 1) + ". " + tasks.get(i).getName() + " : " + linkedTo);
        }
    }

}
