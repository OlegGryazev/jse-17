package ru.gryazev.tm.command.project.sort;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Setting;

@NoArgsConstructor
public class ProjectSortStatus extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sort-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts projects in order of project status.";
    }

    @Override
    public void execute() {
        if (terminalService == null || settingService == null) return;
        settingService.setSetting(new Setting("project-sort", "status"));
        terminalService.print("[OK]");
    }

}
