package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IProjectEndpoint;
import ru.gryazev.tm.error.CrudNotFoundException;

@NoArgsConstructor
public abstract class AbstractTaskCommand extends AbstractCommand {

    @Nullable
    public String getCurrentProjectId() {
        if (settingService == null) return null;
        return settingService.findValueByKey("current-project");
    }

    @Nullable
    public String getProjectId() throws Exception {
        if (serviceLocator == null || terminalService == null) return null;
        @NotNull final String token = getToken();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @Nullable String projectId = getCurrentProjectId();
        if (projectId == null) {
            projectId = projectEndpoint.getProjectId(token, terminalService.getProjectIndex());
        }
        if (projectId == null) throw new CrudNotFoundException();
        return projectId;
    }

}
