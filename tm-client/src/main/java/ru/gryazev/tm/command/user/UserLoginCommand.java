package ru.gryazev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudNotFoundException;

public final class UserLoginCommand extends AbstractCommand {

    public UserLoginCommand() {
        setAllowed(true);
    }

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "User authorization.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null || sessionLocator == null) return;
        @NotNull final User user = terminalService.getUserFromConsole();
        @Nullable final String token = serviceLocator.getSessionEndpoint().createSession(user.getLogin(), user.getPwdHash());
        if (token == null) throw new CrudNotFoundException();
        terminalService.print("[You are logged in]");
        sessionLocator.setToken(token);
    }

}
