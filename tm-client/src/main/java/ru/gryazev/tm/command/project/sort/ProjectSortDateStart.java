package ru.gryazev.tm.command.project.sort;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Setting;

@NoArgsConstructor
public class ProjectSortDateStart extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sort-date-start";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts projects in order of date of project start.";
    }

    @Override
    public void execute() {
        if (terminalService == null || settingService == null) return;
        settingService.setSetting(new Setting("project-sort", "date-start"));
        terminalService.print("[OK]");
    }

}
