package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.ITaskEndpoint;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.error.CrudNotFoundException;

@NoArgsConstructor
public final class TaskViewCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-view";
    }

    @Override
    public String getDescription() {
        return "View selected task.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final String token = getToken();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final int taskIndex = terminalService.getTaskIndex();
        @Nullable String projectId = getProjectId();
        @Nullable final String taskId = taskEndpoint.getTaskId(token, projectId, taskIndex);

        @Nullable final Task task = taskEndpoint.findOneTask(token, taskId);
        if (task == null) throw new CrudNotFoundException();
        terminalService.printTask(task);
    }

}
