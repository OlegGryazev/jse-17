package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractCrudEntity> implements IRepository<T> {

    @NotNull
    protected final Map<String, T> entities = new LinkedHashMap<>();

    @Nullable
    @Override
    public T findOne(@NotNull final String userId, @NotNull final String id) {
        @Nullable final T t = entities.get(id);
        if (t == null || !userId.equals(t.getUserId())) return null;
        return t;
    }

    @Nullable
    @Override
    public T merge(@NotNull final String userId, @NotNull final T t) {
        if (!userId.equals(t.getUserId())) return null;
        entities.put(t.getId(), t);
        return t;
    }

    @NotNull
    @Override
    public List<T> findAll() {
        return new ArrayList<T>(entities.values());
    }

}
