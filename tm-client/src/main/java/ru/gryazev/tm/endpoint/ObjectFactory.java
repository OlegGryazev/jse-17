
package ru.gryazev.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.gryazev.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateUser_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "createUser");
    private final static QName _CreateUserResponse_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "createUserResponse");
    private final static QName _EditUser_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "editUser");
    private final static QName _EditUserResponse_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "editUserResponse");
    private final static QName _FindAllUser_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "findAllUser");
    private final static QName _FindAllUserResponse_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "findAllUserResponse");
    private final static QName _FindCurrentUser_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "findCurrentUser");
    private final static QName _FindCurrentUserResponse_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "findCurrentUserResponse");
    private final static QName _FindOneUser_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "findOneUser");
    private final static QName _FindOneUserResponse_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "findOneUserResponse");
    private final static QName _GetUserId_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "getUserId");
    private final static QName _GetUserIdResponse_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "getUserIdResponse");
    private final static QName _RemoveUserSelf_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "removeUserSelf");
    private final static QName _RemoveUserSelfResponse_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "removeUserSelfResponse");
    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.gryazev.ru/", "Exception");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.gryazev.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateUser }
     * 
     */
    public CreateUser createCreateUser() {
        return new CreateUser();
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link EditUser }
     * 
     */
    public EditUser createEditUser() {
        return new EditUser();
    }

    /**
     * Create an instance of {@link EditUserResponse }
     * 
     */
    public EditUserResponse createEditUserResponse() {
        return new EditUserResponse();
    }

    /**
     * Create an instance of {@link FindAllUser }
     * 
     */
    public FindAllUser createFindAllUser() {
        return new FindAllUser();
    }

    /**
     * Create an instance of {@link FindAllUserResponse }
     * 
     */
    public FindAllUserResponse createFindAllUserResponse() {
        return new FindAllUserResponse();
    }

    /**
     * Create an instance of {@link FindCurrentUser }
     * 
     */
    public FindCurrentUser createFindCurrentUser() {
        return new FindCurrentUser();
    }

    /**
     * Create an instance of {@link FindCurrentUserResponse }
     * 
     */
    public FindCurrentUserResponse createFindCurrentUserResponse() {
        return new FindCurrentUserResponse();
    }

    /**
     * Create an instance of {@link FindOneUser }
     * 
     */
    public FindOneUser createFindOneUser() {
        return new FindOneUser();
    }

    /**
     * Create an instance of {@link FindOneUserResponse }
     * 
     */
    public FindOneUserResponse createFindOneUserResponse() {
        return new FindOneUserResponse();
    }

    /**
     * Create an instance of {@link GetUserId }
     * 
     */
    public GetUserId createGetUserId() {
        return new GetUserId();
    }

    /**
     * Create an instance of {@link GetUserIdResponse }
     * 
     */
    public GetUserIdResponse createGetUserIdResponse() {
        return new GetUserIdResponse();
    }

    /**
     * Create an instance of {@link RemoveUserSelf }
     * 
     */
    public RemoveUserSelf createRemoveUserSelf() {
        return new RemoveUserSelf();
    }

    /**
     * Create an instance of {@link RemoveUserSelfResponse }
     * 
     */
    public RemoveUserSelfResponse createRemoveUserSelfResponse() {
        return new RemoveUserSelfResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "createUser")
    public JAXBElement<CreateUser> createCreateUser(CreateUser value) {
        return new JAXBElement<CreateUser>(_CreateUser_QNAME, CreateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EditUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "editUser")
    public JAXBElement<EditUser> createEditUser(EditUser value) {
        return new JAXBElement<EditUser>(_EditUser_QNAME, EditUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EditUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "editUserResponse")
    public JAXBElement<EditUserResponse> createEditUserResponse(EditUserResponse value) {
        return new JAXBElement<EditUserResponse>(_EditUserResponse_QNAME, EditUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindAllUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "findAllUser")
    public JAXBElement<FindAllUser> createFindAllUser(FindAllUser value) {
        return new JAXBElement<FindAllUser>(_FindAllUser_QNAME, FindAllUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindAllUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "findAllUserResponse")
    public JAXBElement<FindAllUserResponse> createFindAllUserResponse(FindAllUserResponse value) {
        return new JAXBElement<FindAllUserResponse>(_FindAllUserResponse_QNAME, FindAllUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindCurrentUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindCurrentUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "findCurrentUser")
    public JAXBElement<FindCurrentUser> createFindCurrentUser(FindCurrentUser value) {
        return new JAXBElement<FindCurrentUser>(_FindCurrentUser_QNAME, FindCurrentUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindCurrentUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindCurrentUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "findCurrentUserResponse")
    public JAXBElement<FindCurrentUserResponse> createFindCurrentUserResponse(FindCurrentUserResponse value) {
        return new JAXBElement<FindCurrentUserResponse>(_FindCurrentUserResponse_QNAME, FindCurrentUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindOneUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "findOneUser")
    public JAXBElement<FindOneUser> createFindOneUser(FindOneUser value) {
        return new JAXBElement<FindOneUser>(_FindOneUser_QNAME, FindOneUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindOneUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "findOneUserResponse")
    public JAXBElement<FindOneUserResponse> createFindOneUserResponse(FindOneUserResponse value) {
        return new JAXBElement<FindOneUserResponse>(_FindOneUserResponse_QNAME, FindOneUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "getUserId")
    public JAXBElement<GetUserId> createGetUserId(GetUserId value) {
        return new JAXBElement<GetUserId>(_GetUserId_QNAME, GetUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "getUserIdResponse")
    public JAXBElement<GetUserIdResponse> createGetUserIdResponse(GetUserIdResponse value) {
        return new JAXBElement<GetUserIdResponse>(_GetUserIdResponse_QNAME, GetUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserSelf }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserSelf }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "removeUserSelf")
    public JAXBElement<RemoveUserSelf> createRemoveUserSelf(RemoveUserSelf value) {
        return new JAXBElement<RemoveUserSelf>(_RemoveUserSelf_QNAME, RemoveUserSelf.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserSelfResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserSelfResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "removeUserSelfResponse")
    public JAXBElement<RemoveUserSelfResponse> createRemoveUserSelfResponse(RemoveUserSelfResponse value) {
        return new JAXBElement<RemoveUserSelfResponse>(_RemoveUserSelfResponse_QNAME, RemoveUserSelfResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gryazev.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

}
