package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Setting;

public interface ISettingRepository extends IRepository<Setting> {

    @Nullable
    public String findValueByKey(@NotNull String key);

    @Nullable
    public Setting findOneByKey(@NotNull String key);

    @Nullable
    public Setting merge(@NotNull Setting setting);

    @Nullable
    public Setting persist(@NotNull Setting setting);

}
