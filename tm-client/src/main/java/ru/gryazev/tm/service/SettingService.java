package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.ISettingRepository;
import ru.gryazev.tm.api.service.ISettingService;
import ru.gryazev.tm.entity.Setting;

@AllArgsConstructor
public class SettingService extends AbstractService<Setting> implements ISettingService {

    @Getter
    @NotNull
    final ISettingRepository repository;

    @Nullable
    @Override
    public String findValueByKey(@Nullable String key) {
        if (key == null || key.isEmpty()) return null;
        return repository.findValueByKey(key);
    }

    @Override
    public @Nullable Setting setSetting(@NotNull Setting setting) {
        if (!isEntityValid(setting)) return null;
        @Nullable final Setting existingSetting = repository.findOneByKey(setting.getKey());
        if (existingSetting == null) return repository.persist(setting);
        existingSetting.setValue(setting.getValue());
        return repository.merge(existingSetting);
    }

    @Override
    public boolean isEntityValid(@Nullable Setting setting) {
        return setting != null && setting.getKey() != null;
    }

}
