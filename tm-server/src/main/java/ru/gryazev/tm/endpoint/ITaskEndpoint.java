package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.dto.Task;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @Nullable
    public Task findOneTask(@Nullable String token, @Nullable String taskId) throws Exception;

    @Nullable
    public Task createTask(@Nullable String token, @Nullable Task task) throws Exception;

    public void removeAllTask(@Nullable String token) throws Exception;

    @Nullable
    public Task editTask(@Nullable String token, @Nullable Task task) throws Exception;

    public void removeTask(@Nullable String token, @Nullable String taskId) throws Exception;

    @Nullable
    public String getTaskId(@Nullable String token,
                            @Nullable String projectId,
                            int taskIndex) throws Exception;

    @NotNull
    public List<Task> findTaskByName(@Nullable String token, @Nullable String taskName) throws Exception;

    @NotNull
    public List<Task> findTaskByDetails(@Nullable String token, @Nullable String taskDetails) throws Exception;

    @NotNull
    public List<Task> listTaskByProject(@Nullable String token, @Nullable String projectId) throws Exception;

    @NotNull
    public List<Task> findTaskByProjectSorted(@Nullable String token,
                                              @Nullable String projectId,
                                              @Nullable String sortType) throws Exception;

    @NotNull
    public List<Task> listTaskUnlinked(@Nullable String token) throws Exception;

    public void removeByProjectId(@Nullable String token, @Nullable String projectId) throws Exception;

    @NotNull
    public List<Task> listTaskUnlinkedSorted(@Nullable String token, @Nullable String sortType) throws Exception;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator);

}
