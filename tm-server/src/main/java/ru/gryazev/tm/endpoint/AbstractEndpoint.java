package ru.gryazev.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.util.CryptUtil;

import javax.jws.WebMethod;

public abstract class AbstractEndpoint {

    @NotNull
    @WebMethod(exclude = true)
    Session getSessionFromToken(@NotNull final String token, @NotNull final ServiceLocator serviceLocator) throws Exception {
        @NotNull final String key = serviceLocator.getPropertyService().getProperty("key");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = CryptUtil.decrypt(token, key);
        @NotNull final Session session = objectMapper.readValue(json, Session.class);
        return session;
    }

}
