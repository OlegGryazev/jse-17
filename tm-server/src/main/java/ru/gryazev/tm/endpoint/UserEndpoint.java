package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.dto.User;
import ru.gryazev.tm.entity.DtoFactory;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@WebService(endpointInterface = "ru.gryazev.tm.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    @Nullable
    public User findOneUser(@Nullable final String token, @Nullable final String userId) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        @Nullable final UserEntity userEntity = serviceLocator.getUserService().findOne(userId);
        return DtoFactory.convertEntityToDto(userEntity);
    }

    @Nullable
    public User findCurrentUser(@Nullable final String token) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        @Nullable final UserEntity userEntity = serviceLocator.getUserService().findOne(session.getUserId());
        return DtoFactory.convertEntityToDto(userEntity);
    }

    @Nullable
    @Override
    public User createUser(@Nullable User user) throws Exception {
        if (serviceLocator == null || user == null) return null;
        @Nullable final UserEntity userEntity = User.toUserEntity(serviceLocator, user);
        @Nullable final UserEntity createdUserEntity = serviceLocator.getUserService().create(userEntity);
        return DtoFactory.convertEntityToDto(createdUserEntity);
    }

    @Nullable
    @Override
    public User editUser(@Nullable final String token, @Nullable final User user) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        @Nullable final UserEntity editedUserEntity = serviceLocator.getUserService()
                .edit(session.getUserId(), User.toUserEntity(serviceLocator, user));
        return DtoFactory.convertEntityToDto(editedUserEntity);
    }

    @NotNull
    @Override
    public List<User> findAllUser(@Nullable String token) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        @NotNull final List<User> users = new ArrayList<>();
        serviceLocator.getUserService().findAll().forEach(o -> users.add(DtoFactory.convertEntityToDto(o)));
        return users;
    }

    @Override
    public void removeUserSelf(@Nullable String token) throws Exception {
        if (serviceLocator == null || token == null) return;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getUserService().remove(session.getUserId());
    }

    @Nullable
    @Override
    public String getUserId(@Nullable String token, int userIndex) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getUserService().getUserId(userIndex);
    }

    @Override
    @WebMethod(exclude = true)
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
