package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.UserEntity;

import java.util.List;

public interface IUserService {

    @Nullable
    UserEntity create(@Nullable UserEntity userEntity) throws Exception;

    @Nullable
    String getUserId(int userIndex) throws Exception;

    @Nullable UserEntity findOne(@Nullable String id);

    @Nullable
    UserEntity edit(@Nullable String userId, @Nullable UserEntity userEntity) throws Exception;

    void remove(@Nullable String id) throws Exception;

    void removeAll() throws Exception;

    @NotNull
    List<UserEntity> findAll() throws Exception;

}
