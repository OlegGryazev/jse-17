package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.entity.AbstractCrudEntity;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.UserEntity;

import java.util.List;

public interface IRepository <T extends AbstractCrudEntity> {

    public void persist(@NotNull T t);

    public List<T> findAll();

    public void removeAll();

    public void removeAllByUserId(@NotNull String id);

    public void merge(@NotNull T t) throws Exception;

}
