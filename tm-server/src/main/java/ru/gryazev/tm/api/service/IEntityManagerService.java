package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IEntityManagerService {

    @NotNull
    public EntityManager getEntityManager();

}
