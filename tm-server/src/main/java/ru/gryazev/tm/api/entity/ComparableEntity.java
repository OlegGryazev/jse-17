package ru.gryazev.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.enumerated.Status;

import java.util.Date;

public interface ComparableEntity {

    @Nullable
    public Date getDateStart();

    @Nullable
    public Date getDateFinish();

    public @NotNull Status getStatus();

    public Long getCreateMillis();

}
