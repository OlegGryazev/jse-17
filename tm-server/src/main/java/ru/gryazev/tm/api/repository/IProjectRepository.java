package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.ProjectEntity;

import java.util.List;

public interface IProjectRepository extends IRepository<ProjectEntity> {
    
    @Nullable
    public ProjectEntity findOneById(@NotNull String userId, @NotNull String id);

    public void removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    public List<ProjectEntity> findAllByName(@NotNull String userId, @NotNull String projectName);

    @NotNull
    public List<ProjectEntity> findAllByDetails(@NotNull String userId, @NotNull String projectDetails);

    @NotNull
    public List<ProjectEntity> findAllByUserId(@NotNull String userId);

    @NotNull
    public List<ProjectEntity> findAllByUserIdSorted(@NotNull String userId, @NotNull String sqlSortType);
    
}
