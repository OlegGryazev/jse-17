package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.UserEntity;

import java.util.List;

public interface IUserRepository extends IRepository<UserEntity> {

    @Nullable
    public UserEntity findOneById(@NotNull String id);

    public UserEntity findByLoginAndPwd(@NotNull String login, @NotNull String pwdHash);
    
}
