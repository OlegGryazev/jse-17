package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    public String getProperty(@NotNull String propertyKey) throws Exception;

}
