package ru.gryazev.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.IMessageProducerService;
import ru.gryazev.tm.dto.AbstractCrudDTO;
import ru.gryazev.tm.entity.AbstractCrudEntity;
import ru.gryazev.tm.entity.DtoFactory;
import ru.gryazev.tm.service.MessageProducerService;

import javax.jms.JMSException;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

public class AbstractCrudEntityListener {

    @NotNull final IMessageProducerService messageProducerService;

    public AbstractCrudEntityListener() throws JMSException {
        this.messageProducerService = new MessageProducerService();
    }

    @PostLoad
    public void onPostLoad(AbstractCrudEntity entity) throws Exception {
        sendMessage(entity, "LOAD");
    }

    @PrePersist
    public void onPrePersist(AbstractCrudEntity entity) throws Exception {
        sendMessage(entity, "START PERSIST");
    }

    @PostPersist
    public void onPostPersist(AbstractCrudEntity entity) throws Exception {
        sendMessage(entity, "FINISH PERSIST");
    }

    @PreUpdate
    public void onPreUpdate(AbstractCrudEntity entity) throws Exception {
        sendMessage(entity, "START UPDATE");
    }

    @PostUpdate
    public void onPostUpdate(AbstractCrudEntity entity) throws Exception {
        sendMessage(entity, "FINISH UPDATE");
    }

    @PreRemove
    public void onPreRemove(AbstractCrudEntity entity) throws Exception {
        sendMessage(entity, "START REMOVE");
    }

    @PostRemove
    public void onPostRemove(AbstractCrudEntity entity) throws Exception {
        sendMessage(entity, "FINISH REMOVE");
    }

    private void sendMessage(
            @Nullable final AbstractCrudEntity entity,
            @NotNull final String message
    ) throws Exception {
        @NotNull final EntityWrapperForJson wrapper = new EntityWrapperForJson(
                DtoFactory.convertEntityToDto(entity),
                String.format("%td-%<tM-%<ty %<tR:%<tS", new Date()),
                entity == null? null : entity.getClass().getSimpleName(),
                message
        );

        try {
            @NotNull final ObjectMapper mapper = new ObjectMapper();
            @NotNull final String json = mapper.writeValueAsString(wrapper);
            messageProducerService.sendMessage(json);
        } catch (Exception e) {
            throw new Exception("Error during logging operation.", e);
        }
    }

    @Getter
    @XmlRootElement
    @AllArgsConstructor
    @XmlAccessorType(XmlAccessType.FIELD)
    class EntityWrapperForJson {

        private final AbstractCrudDTO entity;

        private final String formattedDate;

        private final String ClassName;

        private final String action;

    }

}
