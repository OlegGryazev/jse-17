package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.api.service.IEntityManagerService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.util.CompareUtil;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
public final class TaskService implements ITaskService {

    @NotNull
    final IEntityManagerService entityManagerService;

    @Nullable
    @Override
    public TaskEntity create(@Nullable final String userId, @Nullable final TaskEntity task) {
        if (userId == null || userId.isEmpty()) return null;
        if (!isEntityValid(task)) return null;
        if (!userId.equals(task.getUser().getId())) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.persist(task);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @Nullable
    @Override
    public TaskEntity edit(@Nullable final String userId, @Nullable final TaskEntity task) throws Exception {
        if (!isEntityValid(task)) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (!userId.equals(task.getUser().getId())) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.merge(task);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @Nullable
    @Override
    public TaskEntity findOne(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final TaskEntity task = taskRepository.findOneById(userId, taskId);
        entityManager.close();
        return task;
    }

    @NotNull
    @Override
    public List<TaskEntity> listTaskUnlinked(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskEntity> tasks = taskRepository.findAllByUserIdUnlinked(userId);
        entityManager.close();
        return tasks;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.removeById(userId, taskId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.removeByProjectId(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public String getTaskId(@Nullable final String userId, @Nullable final String projectId, final int taskIndex) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskIndex < 0) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final List<TaskEntity> tasks = projectId == null ? taskRepository.findAllByUserIdUnlinked(userId) :
                taskRepository.findTasksByProjectId(userId, projectId);
        entityManager.close();
        if (taskIndex >= tasks.size()) return null;
        return tasks.get(taskIndex).getId();
    }

    @NotNull
    @Override
    public List<TaskEntity> findByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskEntity> tasks = taskRepository.findTasksByProjectId(userId, projectId);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> findByName(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (taskName == null || taskName.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskEntity> tasks = taskRepository.findAllByName(userId, taskName);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> findByDetails(@Nullable final String userId, @Nullable final String taskDetails) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (taskDetails == null || taskDetails.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskEntity> tasks = taskRepository.findAllByDetails(userId, taskDetails);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> listTaskByProjectSorted(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String sortType
    ) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlSortType = CompareUtil.getSqlSortType(sortType);
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskEntity> tasks = taskRepository.findAllByProjectIdSorted(userId, projectId, sqlSortType);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> listTaskUnlinkedSorted(@Nullable String userId, @Nullable String sortType) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlSortType = CompareUtil.getSqlSortType(sortType);
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskEntity> tasks = taskRepository.findAllByUserIdUnlinkedSorted(userId, sqlSortType);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskEntity> tasks = taskRepository.findAllByUserId(userId);
        entityManager.close();
        return tasks;
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @NotNull
    @Override
    public List<TaskEntity> findAll() {
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskEntity> tasks = taskRepository.findAll();
        entityManager.close();
        return tasks;
    }

    public boolean isEntityValid(@Nullable final TaskEntity task) {
        if (task == null) return false;
        if (task.getId() == null || task.getId().isEmpty()) return false;
        if (task.getUser() == null || task.getUser().getId() == null || task.getUser().getId().isEmpty()) return false;
        return (task.getName() != null && !task.getName().isEmpty());
    }

}