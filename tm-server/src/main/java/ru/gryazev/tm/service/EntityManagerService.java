package ru.gryazev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.service.IEntityManagerService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerService implements IEntityManagerService {

    @NotNull
    final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("projectmanager");

    @NotNull
    public final EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
