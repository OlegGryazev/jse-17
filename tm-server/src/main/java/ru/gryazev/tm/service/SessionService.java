package ru.gryazev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.ISessionRepository;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.IEntityManagerService;
import ru.gryazev.tm.api.service.ISessionService;
import ru.gryazev.tm.constant.Constant;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.entity.DtoFactory;
import ru.gryazev.tm.entity.SessionEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.repository.SessionRepository;
import ru.gryazev.tm.repository.UserRepository;
import ru.gryazev.tm.util.CryptUtil;
import ru.gryazev.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
public class SessionService implements ISessionService {

    @NotNull
    final IEntityManagerService entityManagerService;
    
    @Nullable
    @Override
    public String create(@Nullable final String login, @Nullable final String pwdHash) throws Exception {
        if (login == null || login.isEmpty()) return null;
        if (pwdHash == null || pwdHash.isEmpty()) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        @Nullable final UserEntity userEntity = userRepository.findByLoginAndPwd(login, pwdHash);
        if (userEntity == null) return null;
        @NotNull final SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setUser(userEntity);
        sessionEntity.setRole(userEntity.getRoleType());
        sessionEntity.setSignature(SignatureUtil.sign(DtoFactory.convertEntityToDto(sessionEntity), Constant.SALT, Constant.CYCLE_COUNT));
        entityManager.getTransaction().begin();
        try {
            sessionRepository.persist(sessionEntity);
            entityManager.getTransaction().commit();
            return encryptSession(sessionEntity);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    private SessionEntity findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        @Nullable final SessionEntity sessionEntity = sessionRepository.findOneById(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return sessionEntity;
    }

    @NotNull
    @Override
    public List<SessionEntity> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        @Nullable final List<SessionEntity> sessionEntities = sessionRepository.findByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return sessionEntities;
    }

    public void validateSession(@Nullable final Session session, @Nullable final RoleType[] roles) throws Exception {
        @NotNull final String message = "Session is not valid!";
        if (session == null) throw new Exception(message);
        if (roles == null || !Arrays.asList(roles).contains(session.getRole())) throw new Exception(message);
        validateSession(session);
    }

    public void validateSession(@Nullable final Session session) throws Exception {
        @NotNull final String message = "Session is not valid!";
        if (session == null) throw new Exception(message);
        if (session.getId().isEmpty()) throw new Exception(message);
        @NotNull final long sessionAliveTime = new Date().getTime() - session.getTimestamp();
        if (sessionAliveTime > Constant.SESSION_LIFETIME) throw new Exception(message);
        if (session.getRole() == null) throw new Exception(message);
        @Nullable final SessionEntity sessionAtServer = findOne(session.getId(), session.getId());
        if (sessionAtServer == null || sessionAtServer.getSignature() == null) throw new Exception(message);
        @Nullable final String actualSessionSignature =  SignatureUtil.sign(session, Constant.SALT, Constant.CYCLE_COUNT);
        if (!sessionAtServer.getSignature().equals(actualSessionSignature)) throw new Exception(message);
    }

    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        try{
            sessionRepository.removeAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        try{
            sessionRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new Exception(e);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    private String encryptSession(@Nullable final SessionEntity sessionEntity) throws Exception {
        if (sessionEntity == null) return null;
        @NotNull final String key = new PropertyService().getProperty("key");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(DtoFactory.convertEntityToDto(sessionEntity));
        return CryptUtil.encrypt(json, key);
    }

}
