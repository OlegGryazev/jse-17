package ru.gryazev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.service.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private final String fileName = "server.properties";

    @NotNull
    public String getProperty(@NotNull final String propertyKey) throws Exception {
        @NotNull final Properties props = new Properties();
        try (final InputStream in = getClass().getClassLoader().getResourceAsStream(fileName)) {
            props.load(in);
        } catch (IOException e) {
            throw new Exception("Corrupted properties.");
        }
        @NotNull final String value = props.getProperty(propertyKey);
        if (value == null) throw new Exception("Corrupted properties.");
        return value;
    }

}
