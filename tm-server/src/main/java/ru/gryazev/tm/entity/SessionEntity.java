package ru.gryazev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "app_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SessionEntity extends AbstractCrudEntity {

    @NotNull
    @ManyToOne
    private UserEntity user;

    @Nullable
    @Enumerated(EnumType.STRING)
    private RoleType role;

    @Nullable
    @JsonIgnore
    private String signature;

    @NotNull
    private Long timestamp = new Date().getTime();

}
